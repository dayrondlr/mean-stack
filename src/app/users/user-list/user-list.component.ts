import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
    this.createNewUser();
  }

  createNewUser() {
    const user: User = {
      name: '',
      email: '',
      phone: {
        work: '',
        mobile: ''
      }
    };

    // By default, a newly-created contact will have the selected state.
    this.userService.selectUser = user;
  }

  /**
    * Get employees
    */
  getUsers(): void {
    this.userService.getUsers()
      .subscribe(res => {
        this.userService.users = res as User[];
      });
  }

  /**
   * User selected.
   * @param user data user pick
   */
  selectUser(user: User): void {
    this.userService.selectUser = user;
  }
}
