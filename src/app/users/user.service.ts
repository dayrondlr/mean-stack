import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  selectUser: User;
  users: User[];

  /**
   * Url from user API
   */
  private URL_API = 'https://dlr-mean.herokuapp.com/api/user/';

  constructor(private http: HttpClient) {
    this.selectUser = new User();

  }

  /**
   * GET API Users
   */
  getUsers() {
    return this.http.get(this.URL_API);
  }

  /**
   * POST API User
   */
  postUser(user: User) {
    return this.http.post(this.URL_API, user);
  }

  /**
   * GET API User
   */
  getUser(_id: string) {
    return this.http.get(this.URL_API + _id);
  }

  /**
   * PUT API User
   */
  putUser(user: User) {
    return this.http.put(this.URL_API + user._id, user);
  }

  /**
   * DELETE API User
   */
  deleteUser(_id: string) {
    return this.http.delete(this.URL_API + _id);
  }
}
