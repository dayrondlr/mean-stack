import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { User } from '../user';

declare var M: any;

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  /**
   * Save data user.
   * @param form data of user.
   */
  postUser(form: NgForm) {
    this.userService.postUser(form.value)
      .subscribe(res => {
        M.toast({ html: res });
        this.userService.getUsers().subscribe(users => this.userService.users = users as User[]);
        form.reset();
      });
  }

  /**
   * Update a user.
   * @param form new data of user.
   */
  putUser(form: NgForm) {
    this.userService.putUser(form.value)
      .subscribe(res => {
        M.toast({ html: res });
        this.userService.getUsers().subscribe(users => this.userService.users = users as User[]);
        form.reset();
      });
  }

  /**
   * Delete a user
   * @param _id ID of user.
   * @param form form to clean
   */
  deleteUser(_id: string, form: NgForm) {
    this.userService.deleteUser(_id)
      .subscribe(res => {
        M.toast({ html: res });
        this.userService.getUsers().subscribe(users => this.userService.users = users as User[]);
        form.reset();
      });
  }
}
