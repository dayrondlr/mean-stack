const express = require('express');
const router = express.Router();
const userCtrl=require('../controllers/user.ctrl');

router.get('/user', userCtrl.getUsers);
router.post('/user', userCtrl.postUser);
router.get('/user/:id', userCtrl.getUser);
router.put('/user/:id', userCtrl.putUser);
router.delete('/user/:id', userCtrl.deleteUser);

module.exports = router;