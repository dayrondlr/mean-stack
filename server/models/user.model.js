const mongoose = require('mongoose');
const { Schema } = mongoose;

let userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        mobile: { type: String },
        work: { type: String }
    },
    state: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('user', userSchema);