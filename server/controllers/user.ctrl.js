const User = require('../models/user.model');
const _ = require('underscore');

var userCtrl = {};

// Error handler used by all endpoints.
function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({ "error": message });
}

/**
 * Get all users.
 */
userCtrl.getUsers = async (req, res) => {
    await User.find({ state: true }, 'name email phone.mobile phone.work').exec((err, users) => {
        if (err) {
            return handleError(res, err.message, "Failed to get users.");
        }

        res.json(users);
    });
}

/**
 * Add a new user.
 */
userCtrl.postUser = async (req, res) => {
    let user = new User(req.body);
    await user.save((err, user) => {
        if (err) {
            return handleError(res, err.message, "Failed to create new user.");
        }
        res.status(201).json(user.name);
    });
}

/**
 * Get a user for ID. 
 */
userCtrl.getUser = async (req, res) => {
    await User.findById(req.params.id, 'name email phone.mobile phone.work').exec((err, user) => {
        if (err) {
            return handleError(res, err.message, "Failed to get user.");
        }

        res.status(200).json(user);
    });
}

/**
 * Update a user for ID.
 */
userCtrl.putUser = async (req, res) => {
    let user=new User(req.body);
    await User.findByIdAndUpdate(req.params.id, { $set: _.pick(user, 'name', 'email', 'phone') }, { new: true }).exec((err, userUpdate) => {
        if (err) {
            return handleError(res, err.message, "Failed to update user.");
        }

        res.status(201).json(user.name);
    });
}

/**
 * Delete a user for ID.
 */
userCtrl.deleteUser = async (req, res) => {
    let putEstate = {
        state: false
    }
    await User.findByIdAndUpdate(req.params.id, putEstate, { new: true }).exec((err, userDelete) => {
        if (err) {
            return handleError(res, err.message, "Failed to delete user.");
        }

        res.status(201).json(userDelete.name);
    });
}
module.exports = userCtrl;