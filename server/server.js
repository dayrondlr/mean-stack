const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require('cors');
const path = require('path');

var app = express();
app.use(bodyParser.json());

// Proxy configuration
app.use(cors({ origin: 'http://localhost:4200' || `${process.env.URL}` }));

// Create link to Angular build directory
app.use(express.static(path.resolve(__dirname, '../dist')));

// Routes
app.use('/api', require('./routes/user.routes'));

//Enviorment
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
let urlDB;

if (process.env.NODE_ENV === 'dev') {
  urlDB = 'mongodb://localhost:27017/users';
} else {
  urlDB = process.env.MONGO_URI;
}
process.env.URLDB = urlDB;

// Connect to the database before starting the application server.
mongoose.connect(process.env.URLDB, { useNewUrlParser: true }, (err, res) => {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  console.log(`Database connection ok: ${process.env.MONGO_URI}`);

  // Initialize the app.
  let server = app.listen(process.env.PORT || 3000, function () {
    let port = server.address().port;
    console.log(`Server running on: http://localhost:${port}`);
  });
});